# DAO User

## Make it work

* Clone the repository.
* Open folder with VSCode.
* Right click and Run `src > main > java > ... > Main.java`.
* Open http://localhost:8080 in the browser.

## Objective

This project is very similar to the previous one (DAO Login), but in this case, we are going to use and explain the [DAO pattern](https://www.baeldung.com/java-dao-pattern) in deep.

Instead of checking if the user exists, we will retrieve the entire user row from the database table and transform it to a Java Class. If the user is null, then we know that it does not exist.

We have also removed the option of login against the properties files, so DaoUserProperties does not exist.

## Explaination

### DAO Pattern

Direct Access Object (DAO) pattern aims to abstract the database logic to the rest of the application, usually transforming the table rows into java objects. That way, the rest of the business logic works directly with objects and the only part of the application that knows how to work with tables, rows and columns are the DAO classes,

### JavaBeans

In order to do that, we need to create an Object for each table. That object will have at least one variable for each column (unless we don't need a specific column). In this case, ```domain.user.model.User``` will be the class that represents our user table. This type of objects are called JavaBean. In order to be a JavaBean, the object has some restrictions:

* (1) It needs a default no argumented constructor (even if empty). This will be called in the views for example.

```java
public User(){}
```

* (2) **serializable**: It is useful to validate the version of the object. It is usually send to check if we are using the same version of the object, for example when sending and when receiving the object. If we have different serialVersionUIDs we are using a different version of the object and we can have problems.

```java
public class User implements java.io.Serializable /*2nd characteristic*/{
  private static final long serialVersionUID = 3834633934831160740L;
  ...
}
```

* (3) **Private properties**: They are usually related to the columns of the table it represents. Private to provide a complete control of the fields. *Nobody external can access them without passing through my functions*.

```java
...
private int userId = 0;
private String username = null;
...
```

* (4) **Getters and setters**: in order to make properties abailable to other functions without losing the control, getters and setters have to be implemented.

```java
public Integer getUserId() {
  return userId;
}
public void setUserId(Integer userId) {
  this.userId = userId;
}
public String getUsername() {
  return username;
}
public void setUsername(String username) {
  this.username = username;
}
```

### Java Beans in the views

If the controller stores the user in the session, we could access to the object parameters easyly from a view:

```jsp
<ul>
  <li>userId:
    <c:out value="${sessionScope.user.userId}" default="Unknown Id" />
  </li>
  ...
</ul>
```

### DAOUser instead of DAOLogin

With DaoLogin we checked the username and password, now the facade returns the ```User JavaBean```. If it is null, the username and password provided are not correct.

We can see that now LoginController calls UserFacade instead of LoginFacade (that does not exist anymore):

```java
// Check if the user exists in the database
UserFacade uf = new UserFacade();
if (username != null && password != null) {
  user = uf.loadUser(username, password);
}
```

**We are still using FacadePattern, because it will be easyer to adapt to future Database changes than if we don't use it (and to learn how to use it). But in this case it is not very usefull for us right now.**

## MySQLConfig file

We talked about this file in the previous application, but you can find an explaination in deep below.

It is used to load Database configuration (db.properties) and to open and close MySQL Connections.

So DaoUserMySQL will call this class to open and close MySQL connections when accessing the database.

**In real world scenarios, it is not wise to create a connection and close it every time a request is made. There is usually a connection pull and it is given to different request as they arrive (it is much more efficient and faster). Nevertheless and to simplify the code, this time we will do it int the *ineficient* way.**

## Singleton pattern

**MySQLConfig** should load the configuration once, the first time it is loaded. That way it will be more efficient (the only negative aspect is that we cannot change database configuration on the fly).

In order to do that, we will use the Singleton pattern. With the singleton patterns, we cannot do ```MySQLConfig config = new MySQLConfig();```, we have to do ```MySQLConfig config = MySQLConfig.getInstance()```. It will give as the existing **MySQLConfig** class, and if it does not exist, it will create one and give it back to us. That way, there will only be one single **MySQLConfig** class in the system (that is why its called Singleton).

In order to do that, the constructor has to be private, otherwise anywan could call ```new MySQLConfig();```.

```java
private MySQLConfig() {
  ...
}
```

But if the constructor is private, how does any other object call a singleton class? We have to create a ```public static``` function (functions that can be called without calling that constructor) that will create or return us the instance:

```java
private static MySQLConfig myconfig;
...
public static MySQLConfig getInstance() {
    if (myconfig == null) {
      myconfig = new MySQLConfig();
    }
    return myconfig;
  }
```

So only **MySQLConfig** can create a **MySQLConfig** instance.

## Exercise

We have explained many elements about the development of web application (libraries, classes, patterns...). Now is your turn to evolve this small application.

This is what you have to do:

* Create a new project taking the code from this one as a base (call it mvc-exercise-1 for example).
* Add CRUD functionalities (Create, Read, Update and Delete):
  * Create user (Create).
  * View User List (Read).
  * Single user information (Read).
  * Edit user (Update).
  * Remove User (Delete).
* Add a UserController that responds to the following actions:
  * ```GET /user``` & ```/GET /user?action=list``` should do the same: Dispatch ```user_list.jsp``` showing the list of all users.
  * ```GET /user?action=view&userId={id}``` should dispatch ```user.jsp``` showing user's data.
  * ```GET /user?action=create``` should dispatch ```user_form.jsp``` (empty).
  * ```POST /user?action=create``` should store user in DB and redirect to ```/user?action=view&userId={id}```.
  * ```GET /user?action=edit&userId={id}``` should dispatch ```user_form.jsp``` filled with user's data.
  * ```POST /user?action=edit&userId={id}``` should update user in DB and redirect to ```/user?action=view&userId={id}```.
  * ```GET /user?action=delete&userId={id}``` should delete user from DB and redirect to ```/user?action=list```.
* Create/modify any needed file.

  * Functions in DAO (Model)
  * JSP (View)
  * Servlets (Controller)

* index.jsp should be the only JSP file that appears in the URL. (Use dispatch and redirect to Controllers instead of to JSPs).
* Make it Muti-lingual.

## Next and before

* Before: [10-dao-login](https://gitlab.com/mgep-web-engineering-1/javaee/10-dao-login)

* Next: [12-friendly-urls](https://gitlab.com/mgep-web-engineering-1/javaee/12-friendly-urls)